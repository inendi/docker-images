#!/bin/sh

cat <<EOF

workflow:
  rules:
    - if: \$CI_MERGE_REQUEST_IID
    - if: \$CI_COMMIT_BRANCH

stages:
  - build
  - stop

EOF

if [ $REBUILD_ALL -eq 1 ]; then
    MODIFIED_DOCKERFILES=`find * -type f -wholename "*_docker/Dockerfile"`
else
  git fetch -a
  COMMIT_BEFORE="origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
  if [ -z $CI_MERGE_REQUEST_TARGET_BRANCH_NAME ]; then
    COMMIT_BEFORE=$CI_COMMIT_BEFORE_SHA
  fi
  MODIFIED_DOCKERFILES=`git diff-tree --no-commit-id --name-only -r $COMMIT_BEFORE -r $CI_COMMIT_SHA | grep "_docker/Dockerfile"`
fi

for dockerfile in $MODIFIED_DOCKERFILES
do
  if [ ! -f $dockerfile ]; then
      continue # Ignore deleted files
  fi
  job_folder=`echo $dockerfile | cut -d"/" -f 1`
  job_name="${job_folder%_docker}"
cat <<EOF

${job_name}:
  stage: build
  tags:
    - privileged-docker
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  before_script:
    - mkdir -p /kaniko/.docker
    - cd ${job_folder}
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "gitlab-ci-token" "${CI_JOB_TOKEN}" | base64 | tr -d '\n')\"}}}" > $DOCKER_CONFIG/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --build-arg=CI_PROJECT_DIR=${CI_PROJECT_DIR}
      --dockerfile "Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}/${job_name}:\$CI_COMMIT_REF_NAME"
      --destination "${CI_REGISTRY_IMAGE}/${job_name}:\$CI_COMMIT_SHORT_SHA"
      --force
  environment:
    on_stop: ${job_name}_stop
    name: review/\$CI_COMMIT_REF_SLUG
    url: https://\$CI_MERGE_REQUEST_PROJECT_URL

${job_name}_stop: # Delete container image when branch is merged
  stage: stop
  tags:
   - privileged-docker
  before_script:
    - wget "https://github.com/genuinetools/reg/releases/download/v0.16.1/reg-linux-amd64" -O ./reg
    - chmod a+x ./reg &&
  script:
      ./reg rm -d --auth-url \$CI_REGISTRY -u \$CI_REGISTRY_USER -p \$CI_REGISTRY_PASSWORD ${job_name}:\$CI_COMMIT_REF_NAME ;
      ./reg rm -d --auth-url \$CI_REGISTRY -u \$CI_REGISTRY_USER -p \$CI_REGISTRY_PASSWORD ${job_name}:\$CI_COMMIT_SHORT_SHA
  environment:
    name: review/\$CI_COMMIT_REF_SLUG
    action: stop
  when: manual
  allow_failure: true

EOF
done
